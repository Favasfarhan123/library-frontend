import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './index.css'
import Root from './routes/root';
import ErrorPage from "./error-page";
import Books from './books';
import Authors from './routes/authors';
import Home from './routes/home';
import Book from './routes/book';
import SignIn from './routes/signin';
import SignUp from './routes/signUp';
import Aboutus from './aboutUs';


const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children:[
      {
        path: "/",
        element: <Home />
      },
      {
        path: '/books',
        element: <Books />
      },
      {
        path: '/authors',
        element: <Authors />
      },
      {
        path: '/books/:bookid',
        element: <Book />
      },
      {
        path:'/signin',
        element: <SignIn />,
      },
        {
          path: '/signup',
          element: <SignUp />
        },
        {
          path: '/about-us',
          element: <Aboutus />
        }
      
    ],
  },
]);
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
