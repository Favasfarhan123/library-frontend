import { Link } from "react-router-dom";
import BookComponent from "./components/booksComponent";

export default function Books() {
    return (

        <main className="container mx-auto">
            <h1 className="mb-3 mt-6">Books</h1>
            <Link to={'/'} className="mt-6">New Adult Fiction </Link>
            <div className="grid grid-cols-5 gap-4 mt-6">
                <BookComponent />
                <BookComponent />
                <BookComponent />
                <BookComponent />
                <BookComponent />
            </div>
           

        </main>
    )
}