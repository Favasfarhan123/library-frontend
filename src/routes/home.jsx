import { Link } from "react-router-dom";

export default function Home() {
    return (

        <main className="container mx-auto flex flex-col justify-between items-center mt-7">
            <h1 className="text-3xl">
                Welcome To BookStore
            </h1>
            <span className="mt-5">Read,Learn and Grow</span>
            <p className="mt-10 ">Welcome to Bookstore, your go-to destination for a diverse selection of books spanning every genre and interest. Whether you’re a casual reader, a dedicated bookworm, or someone seeking rare finds, our carefully curated collection ensures there’s something for everyone. We pride ourselves on fostering a community of readers and providing a space where literature enthusiasts can discover, share, and indulge in their love for books. Our knowledgeable staff is always ready to assist and recommend, making your visit a truly enriching experience. Join us at Bookstore and embark on countless literary adventures!</p>
        </main>
    )
}
