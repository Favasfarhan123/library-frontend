import { Link } from "react-router-dom";

export default function SignUp() {
    return (
        

        <main className="container mx-auto">
            
        <form className="flex flex-col justify-between items-center mt-20">
            <label htmlFor="fname">User Name:</label>
            <br />
            <input className="border-solid border-2 border-black " type="text" id="userName" name="userName" />
            <br />
            <label htmlFor="email">Email:</label>
            <br />
            <input className="border-solid border-2 border-black " type="email" id="email" name="email" /><br />
            <label htmlFor="password">Password:</label><br />
            <input className="border-solid border-2 border-black " type="password" id="password" name="password" /><br />
            <div className="flex flex-row justify-center items-center">
            <button className="border-solid border-2 border-black rounded-lg p-2 bg-black text-white" >Sign Up</button>
            </div>
            
        </form>
       

        </main>
    )
}