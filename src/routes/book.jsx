import { Link } from "react-router-dom";

export default function Book() {
    return (

        <main className="container mx-auto grid grid-cols-2 mt-10">
            <div>
            <img id="bookImage" src="https://covers.openlibrary.org/b/id/14416194-M.jpg" alt="book image" />
            </div>
            <div className="flex flex-col justify-center align-center gap-5">
                <h1>title</h1>
                <span>author name</span>
                <span>123</span>
                <p>Description</p>
                <div>
                <button>Buy Now</button>
                </div>
                
            </div>
            
            
        </main>
    )
}