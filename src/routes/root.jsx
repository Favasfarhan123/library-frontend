import { Link, Outlet } from "react-router-dom";

export default function Root() {
    return (
      
      <>
      <header className="container mx-auto flex flex-row justify-between items-center h-24 border-b-4">
            <Link to={'/'}> 
                BookStore
            </Link>
            <nav>
                <ul className="flex flex-row justify-between items-center gap-2">
                    <li>
                    <Link to={'/'}>Home</Link>
                    </li>
                    <li>
                    <Link to={'/books'}>Books</Link>
                    </li>
                    <li>
                    <Link to={'/authors'}>Authors</Link>
                    </li>
                    <li>
                    <Link to={'/about-us'}>About Us</Link>
                    </li>
                    <li>
                        <Link to={'/signin'}>SignIn</Link>
                    </li>
                </ul>  

            </nav>
      </header>

      <Outlet />
      <footer>

      </footer>
      </>
      )
    }