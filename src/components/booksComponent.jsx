import { Link } from "react-router-dom";

export default function BookComponent() {
    return (

        <main>
            <Link to={'/books/:bookid'}>
            <article className="flex flex-col justify-between items center">
                <img id="booksImage" src="https://www.syndetics.com/index.aspx?isbn=9781250341365&issn=/LC.JPG&client=pickp&type=xw12" alt="Book Cover page" />
                <h2>Book Title</h2>
                <span>Author Name</span>
                <span>123</span>
                <button>Add to Cart</button>

            </article>
            </Link>
            
        </main>
    )
}
